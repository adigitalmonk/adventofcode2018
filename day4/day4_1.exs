defmodule Security do
  @timestamp_r ~r/\[(\d{4}-\d{2}-\d{2} \d{2}:\d{2})\] (.*)/
  def parse_instruction(instruction) do
    # [1518-05-30 00:04] Guard #2417 begins shift
    [_, timestamp, instruction] = Regex.run(@timestamp_r, instruction)
    {:ok, timestamp, _} = DateTime.from_iso8601(timestamp <> ":00Z")
    {timestamp, String.trim(instruction)}
  end

  def rebuild_instructions([head]), do: [parse_instruction(head)]

  def rebuild_instructions([head | tail]) do
    [parse_instruction(head) | rebuild_instructions(tail)]
  end

  def build_naptimes([], naptimes, _, _), do: naptimes

  def build_naptimes([{timestamp, "falls asleep"} | tail], naptimes, guard_id, "") do
    build_naptimes(tail, naptimes, guard_id, timestamp.minute)
  end

  def build_naptimes([{timestamp, "wakes up"} | tail], naptimes, guard_id, start_time) do
    guard_naps = naptimes[guard_id]

    guard_naps =
      Enum.reduce(
        start_time..timestamp.minute,
        guard_naps,
        fn x, acc ->
          Map.put(acc, x, (acc[x] || 0) + 1)
        end
      )

    naptimes = Map.put(naptimes, guard_id, guard_naps)
    build_naptimes(tail, naptimes, guard_id, "")
  end

  @guard_r ~r/.*#(\d+).*/
  def build_naptimes([{_, guard_intro} | tail], naptimes, _, _) do
    [_, guard_id] = Regex.run(@guard_r, guard_intro)
    build_naptimes(tail, Map.put_new(naptimes, guard_id, %{}), guard_id, "")
  end

  def compare_dates(tuple1, tuple2) do
    case DateTime.compare(elem(tuple1, 0), elem(tuple2, 0)) do
      :gt -> false
      :lt -> true
      :eq -> nil
    end
  end

  def find_sleepiest(logs) do
    Enum.reduce(
      logs,
      {"", 0, %{}},
      fn x, acc ->
        minutes_napped = elem(x, 1)
                         |> Map.values()
                         |> Enum.sum()

        case minutes_napped > elem(acc, 1) do
          true -> {elem(x, 0), minutes_napped, elem(x, 1)}
          false -> acc
        end
      end
    )
  end

  def find_sleeper(instructions) do
    instructions
    |> rebuild_instructions
    |> Enum.sort(&compare_dates/2)
    |> build_naptimes(%{}, "", "")
    |> find_sleepiest
  end

  def find_best_minute(instructions) do
    {guard_id, _, minutes} = find_sleeper(instructions)

    {best_minute, _} =
      Enum.reduce(
        minutes,
        {0, 0},
        fn {x, y}, acc ->
          if y > elem(acc, 1), do: {x, y}, else: acc
        end
      )

    String.to_integer(guard_id) * best_minute
  end
end

{:ok, body} = File.read("input.txt")

String.split(body, "\n")
|> Security.find_best_minute()
|> IO.inspect()
