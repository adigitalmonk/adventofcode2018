defmodule Polymer do
  def upcase?(a), do: a == String.upcase(a)

  def pair?(a, b) do
    cond do
      upcase?(a) and not upcase?(b) -> String.downcase(a) == b
      not upcase?(a) and upcase?(b) -> String.upcase(a) == b
      true -> false
    end
  end

  def annihilate([], cleaned, false), do: cleaned

  def annihilate([], cleaned, true), do: cleaned |> simplify

  def annihilate([head], cleaned, false), do: [head | cleaned]

  def annihilate([head], cleaned, true), do: [head | cleaned] |> simplify

  def annihilate([head | [sub | tail]], cleaned, repeat) do
    case pair?(head, sub) do
      true -> annihilate(tail, cleaned, true)
      false -> annihilate([sub | tail], [head | cleaned], repeat)
    end
  end

  def simplify(symbols) do
    annihilate(symbols, [], false)
  end
end

{:ok, body} = File.read("input.txt")

body
|> String.trim()
|> String.graphemes()
|> Polymer.simplify()
|> Enum.count()
|> IO.inspect()
