defmodule Polymer do
  def upcase?(a), do: a == String.upcase(a)

  def pair?(a, b) do
    cond do
      upcase?(a) and not upcase?(b) -> String.downcase(a) == b
      not upcase?(a) and upcase?(b) -> String.upcase(a) == b
      true -> false
    end
  end

  def annihilate([], cleaned, false), do: cleaned

  def annihilate([], cleaned, true),
    do:
      cleaned
      |> simplify

  def annihilate([head], cleaned, false), do: [head | cleaned]

  def annihilate([head], cleaned, true),
    do:
      [head | cleaned]
      |> simplify

  def annihilate([head | [sub | tail]], cleaned, repeat) do
    case pair?(head, sub) do
      true -> annihilate(tail, cleaned, true)
      false -> annihilate([sub | tail], [head | cleaned], repeat)
    end
  end

  def simplify(symbols) do
    annihilate(symbols, [], false)
  end

  def find_best(_, []), do: []

  def find_best(symbols, [head | tail]) do
    IO.write(".")

    count =
      symbols
      |> Enum.filter(&(String.downcase(&1) != String.downcase(head)))
      |> annihilate([], false)
      |> Enum.count()

    [{head, count} | find_best(symbols, tail)]
  end

  def find_best(symbols) do
    find_best(
      symbols,
      [
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "g",
        "h",
        "i",
        "j",
        "k",
        "l",
        "m",
        "n",
        "o",
        "p",
        "q",
        "r",
        "s",
        "t",
        "u",
        "v",
        "w",
        "x",
        "y",
        "z"
      ]
    )
    |> List.keysort(1)
    |> hd
  end
end

{:ok, body} = File.read("input.txt")

body
|> String.trim()
|> String.graphemes()
|> Polymer.find_best()
|> IO.inspect()
