defmodule Polymer do
  def upcase?(a), do: a == String.upcase(a)

  def pair?(a, b) do
    cond do
      upcase?(a) and not upcase?(b) -> String.downcase(a) == b
      not upcase?(a) and upcase?(b) -> String.upcase(a) == b
      true -> false
    end
  end

  def simplify(symbols) do
    List.foldr(symbols, [], fn
      x, [head | tail] -> if pair?(x, head), do: tail, else: [x, head | tail]
      x, [] -> [x]
    end)
  end

  def find_all(symbols) do
    for x <- ?a..?z do
      size =
        symbols
        |> Enum.filter(&(String.downcase(&1) != String.downcase(<<x>>)))
        |> simplify()
        |> byte_size()
      { <<x>>, size }
    end
  end

  ### <Courtesy of Mr. Valim>
  ### https://www.twitch.tv/videos/346879653
  def react(polymer) when is_binary(polymer) do
    discard_and_react(polymer, nil, nil)
  end

  def discard_and_react(polymer, letter1, letter2) when is_binary(polymer) do
    discard_and_react(polymer, [], letter1, letter2)
  end

  defp discard_and_react(<<letter, rest::binary>>, acc, discard1, discard2) 
    when letter == discard1
    when letter == discard2,
    do: discard_and_react(rest, acc, discard1, discard2)

  defp discard_and_react(<<letter1, rest::binary>>, [letter2 | acc], discard1, discard2)
    when abs(letter1 - letter2) == 32,
    do: discard_and_react(rest, acc, discard1, discard2)

  defp discard_and_react(<<letter, rest::binary>>, acc, discard1, discard2), 
    do: discard_and_react(rest, [ letter | acc ], discard1, discard2)

  defp discard_and_react(<<>>, acc, _discard1, _discard2), 
    do: acc |> Enum.reverse() |> List.to_string()
  
  def find_problematic(symbols) do
    ?A..?Z
    |> Task.async_stream(fn letter ->
      {<<letter>>, byte_size(discard_and_react(symbols, letter, letter + 32))}
    end, ordered: false, max_concurrency: 10)
    |> Stream.map(fn {:ok, res} -> res end)
    # |> Enum.to_list()
    |> Enum.min_by(&elem(&1, 1))
  end
  ### </Courtesy of Mr. Valim>

end



File.read!("input.txt") |> Polymer.find_problematic() |> IO.inspect
