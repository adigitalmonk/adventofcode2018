defmodule Polymer do
  def upcase?(a), do: a == String.upcase(a)

  def pair?(a, b) do
    cond do
      upcase?(a) and not upcase?(b) -> String.downcase(a) == b
      not upcase?(a) and upcase?(b) -> String.upcase(a) == b
      true -> false
    end
  end

  def simplify(symbols) do
    List.foldr(symbols, [], fn
      x, [head | tail] -> if pair?(x, head), do: tail, else: [x, head | tail]
      x, [] -> [x]
    end)
  end

  def find_best(_, []), do: []

  def find_best(symbols, [head | tail]) do
    IO.write(".")

    count =
      symbols
      |> Enum.filter(&(String.downcase(&1) != String.downcase(head)))
      |> simplify()
      |> Enum.count()

    [{head, count} | find_best(symbols, tail)]
  end

  def find_best(symbols) do
    find_best(
      symbols, Enum.
      [
        "a",
        "b",
        "c",
        "d",
        "e",
        "f",
        "g",
        "h",
        "i",
        "j",
        "k",
        "l",
        "m",
        "n",
        "o",
        "p",
        "q",
        "r",
        "s",
        "t",
        "u",
        "v",
        "w",
        "x",
        "y",
        "z"
      ]
    )
    |> List.keysort(1)
    |> hd
  end
end

 File.read!("input.txt")
|> String.trim()
|> String.graphemes()
|> Polymer.find_best()
|> IO.inspect()
