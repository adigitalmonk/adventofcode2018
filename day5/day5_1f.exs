defmodule Polymer do
  def upcase?(a), do: a == String.upcase(a)

  def pair?(a, b) do
    cond do
      upcase?(a) and not upcase?(b) -> String.downcase(a) == b
      not upcase?(a) and upcase?(b) -> String.upcase(a) == b
      true -> false
    end
  end

  def simplify_l([], rebuild), do: rebuild

  def simplify_l([ head | tail ], [ c_head | c_tail ]) do
    if pair?(head, c_head) do
      simplify_l(tail, c_tail)
    else
      simplify_l(tail, [ head, c_head | c_tail ])
    end
  end

  def simplify_l([ head | tail ], []) do
    simplify_l(tail, [ head ])
  end

  def simplify(symbols) do
    List.foldr(symbols, [], fn
      x, [head | tail] -> if pair?(x, head), do: tail, else: [x, head | tail]
      x, [] -> [x]
    end)
  end
end

{:ok, body} = File.read("input.txt")

body
|> String.trim()
|> String.graphemes()
|> Polymer.simplify()
#|> Polymer.simplify_l([])
|> Enum.count()
|> IO.inspect()
