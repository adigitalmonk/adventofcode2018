const fs = require('fs');
const data = fs.readFileSync('input.txt').toString().split("");

const isUpper = a => { return a == a.toUpperCase(); }

const isMatch = (a, b) => {
    if (isUpper(a) && !isUpper(b)) {
        return a.toLowerCase() == b;
    } else if (!isUpper(a) && isUpper(b)) {
        return a.toUpperCase() == b;
    } else {
        return false;
    }
};

simple = data.reduceRight((acc, x) => {
    if (acc.length) {
        let first = acc.shift();
        if (isMatch(x, first)) {
            return acc;
        } else {
            acc.unshift(first);
            acc.unshift(x);
            return acc;
        }
    } else {
        acc.unshift(x);
        return acc;
    }
}, []);

console.log(simple.length);
