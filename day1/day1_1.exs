defmodule Frequency do
  def calculate([], curr), do: curr

  def calculate([head | tail], curr), do: calculate(tail, curr + String.to_integer(head))
end

{:ok, body} = File.read("input.txt")
freq = String.split(body, "\n") |> Frequency.calculate(0)
IO.puts(freq)
