defmodule Frequency do
  defp loop([], curr, carry, init), do: loop(init, curr, carry, init)

  defp loop([head | tail], curr, carry, init) do
    new_val = curr + String.to_integer(head)

    if MapSet.member?(carry, new_val) do
      new_val
    else
      loop(tail, new_val, MapSet.put(carry, new_val), init)
    end
  end

  def calculate(frequencies) do
    loop(frequencies, 0, MapSet.new(), frequencies)
  end
end

{:ok, body} = File.read("input.txt")
adjustments = String.split(body, "\n")
freq = Frequency.calculate(adjustments)
IO.puts(freq)
