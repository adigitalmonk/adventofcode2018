defmodule Checksum do
  def string_distance(string1, string2) do
    string1_clean = String.downcase(string1) |> String.graphemes()
    string2_clean = String.downcase(string2) |> String.graphemes()

    matches =
      Enum.zip(string1_clean, string2_clean)
      |> Enum.filter(fn {x, y} -> x == y end)

    abs(Enum.count(string1_clean) - Enum.count(matches))
  end

  def string_diff(str1, str2) do
    string1_clean = String.downcase(str1) |> String.graphemes()
    string2_clean = String.downcase(str2) |> String.graphemes()

    Enum.zip(string1_clean, string2_clean)
    |> Enum.filter(fn {x, y} -> x == y end)
    |> Enum.reduce("", fn {x, _}, acc -> acc <> x end)
  end

  def compare_hash([], _), do: nil

  def compare_hash([head | tail], id) do
    case string_distance(head, id) == 1 do
      true ->
        string_diff(head, id)

      false ->
        compare_hash(tail, id)
    end
  end

  def find_letters([]), do: nil

  def find_letters([head | tail]) do
    case compare_hash(tail, head) do
      nil ->
        find_letters(tail)

      x ->
        x
    end
  end
end

{:ok, body} = File.read("input.txt")
boxes = String.split(body, "\n")
Checksum.find_letters(boxes) |> IO.puts()
