defmodule Checksum do
  def hash(values) do
    calc_hash(values, 0, 0)
  end

  defp calc_hash([], twos, threes), do: twos * threes

  defp calc_hash([head | tail], twos, threes) do
    counts = count_alpha(head)

    calc_hash(
      tail,
      twos + if(check_count(counts, 2), do: 1, else: 0),
      threes + if(check_count(counts, 3), do: 1, else: 0)
    )
  end

  defp count_parts([], counts), do: counts

  defp count_parts([head | tail], counts) do
    count_parts(tail, Map.put(counts, head, (counts[head] || 0) + 1))
  end

  defp count_alpha(str) do
    String.downcase(str)
    |> String.graphemes()
    |> count_parts(%{})
  end

  defp check_count(totals, n) do
    Enum.filter(totals, fn {_, y} -> y == n end)
    |> Enum.count() > 0
  end
end

{:ok, body} = File.read("input.txt")
boxes = String.split(body, "\n")
Checksum.hash(boxes)
|> IO.puts()
