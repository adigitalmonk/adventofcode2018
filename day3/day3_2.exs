defmodule Cloth do
  def parse_instruction(instruction) do
    [_, x, y, x_delta, y_delta] =
      Regex.run(
        ~r/\d+\s@\s(\d+),(\d+):\s(\d+)x(\d+)/,
        instruction
      )

    [x, y, x_delta, y_delta] = [
      String.to_integer(x),
      String.to_integer(y),
      String.to_integer(x_delta),
      String.to_integer(y_delta)
    ]

    [x, x + x_delta, y, y + y_delta]
  end

  defp hash(x, y) do
    Enum.join([x, y], "|")
  end

  def build_y(_, _, y2, y2, full, overlaps), do: [full, overlaps]

  def build_y(x1, x2, y1, y2, full, overlaps) do
    coord = hash(x1, y1)
    overlaps = if MapSet.member?(full, coord), do: MapSet.put(overlaps, coord), else: overlaps
    full = MapSet.put(full, coord)
    build_y(x1, x2, y1 + 1, y2, full, overlaps)
  end

  def build_x(x2, x2, _, _, full, overlaps), do: [full, overlaps]

  def build_x(x1, x2, y1, y2, full, overlaps) do
    [full, overlaps] = build_y(x1, x2, y1, y2, full, overlaps)
    build_x(x1 + 1, x2, y1, y2, full, overlaps)
  end

  def build_grid(instruction_set, full, overlaps) do
    [x1, x2, y1, y2] = parse_instruction(instruction_set)
    build_x(x1, x2, y1, y2, full, overlaps)
  end

  def get_overlap([head], full, overlaps), do: build_grid(head, full, overlaps)

  def get_overlap([head | tail], full, overlaps) do
    [full, overlaps] = build_grid(head, full, overlaps)
    get_overlap(tail, full, overlaps)
  end

  def in_range?(x1, x2, y1, y2, overlaps) do
    Enum.any?(x1..x2, fn x ->
      Enum.any?(y1..y2, fn y -> MapSet.member?(overlaps, hash(x, y)) end)
    end)
  end

  def find_discrete([head | tail], overlaps) do
    [x1, x2, y1, y2] = parse_instruction(head)

    if in_range?(x1, x2, y1, y2, overlaps) do
      find_discrete(tail, overlaps)
    else
      head
    end
  end

  def overlap(instructions) do
    [_, overlaps] = get_overlap(instructions, MapSet.new(), MapSet.new())
    overlaps |> IO.inspect()
    outlier = find_discrete(instructions, overlaps)
    [outlier, overlaps]
  end
end

{:ok, body} = File.read("input.txt")
instructions = String.split(body, "\n")
[outlier, overlaps] = Cloth.overlap(instructions)
overlaps |> MapSet.size() |> IO.inspect()
outlier |> IO.inspect()
